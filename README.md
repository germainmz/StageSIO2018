<h1>Stage – Développement d’un outil de facturation</h1>

<b>Durée du stage :</b> 1 mois<br/>
<b>Objectif du stage :</b> Développement d’un outil web permettant la génération de factures et de devis.  Cet outil sera destiné à un usage interne, et devra générer des factures conformes à la loi.
<h2>Description du projet</h2>
En temps qu’autoentrepreneur, je dois régulièrement emmètre des factures et des devis pour des clients.<br/>
Pour le moment j’utilise un modèle de document Word que je dois remplir à chaque fois avec les informations du client, et les informations du projet à réaliser. Cette tâche est répétitive et il est facile de faire des erreurs. De plus, toutes les informations sont dans des fichiers séparés et il n’est donc pas possible de faire des calculs sur les montants des différentes factures, ce qui serait particulièrement intéressant.<br/>
L’outil web à réaliser devra donc permettre de lister les factures existantes, d’en créer de nouvelles ou de les éditer (partiellement). Il sera éventuellement possible d’en supprimer, si une facture a été créée par erreur. L’outil devra de plus être capable d’exporter les factures au format PDF. <br/>
<h2>Technologies utilisées</h2>
L’outil web comportera une interface en HTML/CSS avec la librairie Bootstrap. L’outil sera rendu dynamique grâce au langage PHP qui permettra de se connecter à une base de données.<br/>
En fonction de l’avancée du projet, il pourra être intéressant de mettre en place un framework JavaScript moderne, comme Vue.js, pour rendre les pages plus interactives.<br/>
Nous utiliserons aussi Git, afin que le code soit versionné et partagé régulièrement.
<h2>Suivi du stage</h2>
Le stage se fera en partie en « télétravail ». <br/>
Chaque matin à 9h nous ferons un point de 15min afin de voir parler de l’avancée du projet et de définir les priorités de la journée. <br/>
Le Git sera mis à jour régulièrement dans la journée afin que je puisse régulièrement avec accès aux sources.<br/>
