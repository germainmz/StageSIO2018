<?php

    // Variables

$nb = 0; // Correspond au nombre total de factures
$requeteSupprDesign = "";

    // Connexion BDD

try {
    $bdd = new PDO('mysql:host=localhost;dbname=bddgestion;charset=utf8', $user, $mdp);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

    // Classes Factures et Prestation

class facture
{

    private $_num, $_date, $_nom, $_adresse, $_cp, $_ville, $_contact,$_intitule, $_debut, $_fin, $_reglement, $_echeance, $_options, $_factavoir;

    public function facture($num, $date, $nom, $adresse, $cp, $ville, $contact,$intitule, $debut, $fin, $reglement, $echeance, $options, $factavoir)
    {
        $this->_num = $num; $this->_date = $date; $this->_nom = $nom; $this->_adresse = $adresse; $this->_cp = $cp; $this->_ville = $ville; $this->_contact = $contact;
        $this->_intitule = $intitule; $this->_debut = $debut; $this->_fin = $fin; $this->_reglement = $reglement; $this->_echeance = $echeance; $this->_options = $options; $this->_factavoir = $factavoir;
    }

    public function sqlstring()
    {
        return "INSERT INTO `factures` (`idFacture`, `num`, `date`, `nomClient`, `adresse`, `cp`, `ville`, `contact`, `intitule`, `dateDeb`, `dateFin`, `condReglement`, `echeance`, `options`, `factAvoir`) 
                                    VALUES ( NULL, '$this->_num', '$this->_date', '$this->_nom', '$this->_adresse', '$this->_cp', '$this->_ville', '$this->_contact', '$this->_intitule', '$this->_debut', '$this->_fin', '$this->_reglement', '$this->_echeance', '$this->_options', '$this->_factavoir');";
    }

    public function sqlstringmodif($id)
    {
        return "UPDATE `factures` SET `num` = '$this->_num', `date` = '$this->_date', `nomClient` = '$this->_nom', `adresse` = '$this->_adresse', `cp` = '$this->_cp', `ville` = '$this->_ville', `contact` = '$this->_contact', `intitule` = '$this->_intitule', `dateDeb` = '$this->_debut', `dateFin` = '$this->_fin', `condReglement` = '$this->_reglement', `echeance` = '$this->_echeance', `options` = '$this->_options', `factAvoir` = '$this->_factavoir' WHERE `factures`.`idFacture` = '$id' ;";
    }

    public function getnum(){
        return $this->_num;
    }

}

class actionfacture
{

    private $_designation, $_quantite, $_prixunitaire, $_prix;

    public function actionfacture($design, $quant, $prixunit, $total)
    {
        $this->_designation = $design;
        if(empty($quant)==1){ $this->_quantite = 'NULL';} else {$this->_quantite = $quant;}
        if(empty($prixunit)==1){ $this->_prixunitaire = 'NULL';} else {$this->_prixunitaire = $prixunit;}
        $this->_prix = $total;
    }

    public function sqlstring($idfacture)
    {
        return "INSERT INTO `actionsfactures` (`idAction`, `idFacture`, `designation`, `qte`, `prixUnitaire`, `prix`)
                                    VALUES ( NULL , '$idfacture', '$this->_designation', $this->_quantite , $this->_prixunitaire, '$this->_prix');";
    }

    public function sqlstringupdate($id)
    {
        return "UPDATE `actionsfactures` SET `designation` = '$this->_designation', `qte` = '$this->_quantite', `prixUnitaire` = '$this->_prixunitaire', `prix` = '$this->_prix' WHERE `actionsfactures`.`idAction` = '$id' ; ";
    }

}



    // Fonction EnvoyerForm

function envoyerfactures($base){


    try {
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $lafacture = new facture($_POST['dataNum'], $_POST['dataDate'], $_POST['dataNom'], $_POST['dataAdresse'],$_POST['dataCP'], $_POST['dataVille'],$_POST['dataContact'],$_POST['dataIntitule'], $_POST['dataDebut'],$_POST['dataFin'],$_POST['dataReglement'],$_POST['dataEcheance'], $_POST['dataOptions'], $_POST['dataType']);

        $requete = $lafacture->sqlstring();
        $base->exec($requete);
        $last_id = $base->lastInsertId();


        try {

            $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $prestation = new actionfacture($_POST['dataDesignation'], $_POST['dataQte'], $_POST['dataUnitaire'], $_POST['dataTotal']);
            $requeteaction = $prestation->sqlstring($last_id);
            $base->exec($requeteaction);

            if(empty($_POST['dataNbPrestation'])==1){ $nbtotalprestation = NULL;} else {$nbtotalprestation = $_POST['dataNbPrestation'];};


            if ($nbtotalprestation >= 1) {
                $n = 1;
                while ($n <= $nbtotalprestation){

                    $prests = new actionfacture($_POST['dataDesignation'.$n], $_POST['dataQte'.$n], $_POST['dataUnitaire'.$n], $_POST['dataTotal'.$n]);
                    $requeteaction = $prests->sqlstring($last_id);
                    $base->exec($requeteaction);
                    $n++;
                }
            }


            echo "
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <div class=\"alert alert-success\">
                                Facture n°". $_POST['dataNum']. " ajoutée avec succès !
                            </div>
                        </div>
                    </div>";
        }
        catch(PDOException $e)
        {
            echo "
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <div class=\"alert alert-danger\">Erreur !<br><br>"
                                . $requeteaction . "<br>" . $e->getMessage().
                            "</div>
                        </div>
                    </div>";
        }
    }
    catch(PDOException $e)
    {
        echo "
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <div class=\"alert alert-danger\">Erreur !<br><br>"
                                . $requete . "<br>" . $e->getMessage().
                            "</div>
                        </div>
                    </div>";
    }
}

function modiffactures($base){


    try {
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $lafacture = new facture($_POST['dataNum'], $_POST['dataDate'], $_POST['dataNom'], $_POST['dataAdresse'],$_POST['dataCP'], $_POST['dataVille'],$_POST['dataContact'],$_POST['dataIntitule'], $_POST['dataDebut'],$_POST['dataFin'],$_POST['dataReglement'],$_POST['dataEcheance'], $_POST['dataOptions'], $_POST['dataType']);

        $requetefact = $lafacture->sqlstringmodif($_POST['dataIDFacture']);
        $base->exec($requetefact);
        $idFacture = $_POST['dataIDFacture'];


            try {

                // UPDATE de la première prestation

                $actionune = new actionfacture($_POST['dataDesignationExist'], $_POST['dataQteExist'], $_POST['dataUnitaireExist'], $_POST['dataTotalExist']);
                $requeteactionupd = $actionune->sqlstringupdate($_POST['dataIDAction']);
                $base->exec($requeteactionupd);

                // Suppression des prestations "décochées"

                if($_POST['dataNbSupprim'] != NULL){
                    $nbPrestationsuppr = $_POST['dataNbSupprim'] - 1;
                    $n = 0;
                    while ($n <= $nbPrestationsuppr) {
                        $idActionSupprime = "dataidActionSuppr".$n."";
                        $requetesuppr = "DELETE FROM `actionsfactures` WHERE `actionsfactures`.`idAction` = " . $_POST[$idActionSupprime] . ";";
                        $base->exec($requetesuppr);
                        echo "
                                <div class=\"row\">
                                    <div class=\"col-lg-12\">
                                        <div class=\"alert alert-success\">
                                        Prestation n°$_POST[$idActionSupprime] supprimée avec succès !
                                        </div>
                                    </div>
                                </div>";
                        $n++;
                    }
                }

                  // UPDATE des autres prestations existantes

                if($_POST['dataPrestExistante'] != NULL){
                    $nbPrestationExist = $_POST['dataPrestExistante'];
                    $n = 1;
                    while ($n <= $nbPrestationExist) {

                        $design = $_POST["dataDesignationExist".$n.""];
                        $quante = $_POST["dataQteExist".$n.""];
                        $uni = $_POST["dataUnitaireExist".$n.""];
                        $tot = $_POST["dataTotalExist".$n.""];


                        $autresactions = new actionfacture($design, $quante, $uni, $tot);
                        $requeteactionupd = $autresactions->sqlstringupdate($_POST['dataIDAction'.$n]);
                        $base->exec($requeteactionupd);

                        $n++;
                    }
                    echo "
                                <div class=\"row\">
                                    <div class=\"col-lg-12\">
                                        <div class=\"alert alert-success\">
                                        Prestations mises à jour avec succès !
                                        </div>
                                    </div>
                                </div>";


                }


                // INSERT INTO des nouvelles prestations

                if($_POST['dataPrestExistante'] != NULL && $_POST['dataPrestExistante'] < $_POST['dataNbPrestation']){
                    $nbPrestationTotal = $_POST['dataNbPrestation'];
                    $n = $_POST['dataPrestExistante'] + 1;
                    while ($n <= $nbPrestationTotal) {

                        $design = $_POST["dataDesignation".$n.""];
                        $quante = $_POST["dataQte".$n.""];
                        $uni = $_POST["dataUnitaire".$n.""];
                        $tot = $_POST["dataTotal".$n.""];

                        $nouvellesactions = new actionfacture($design, $quante, $uni, $tot);
                        $requeteinsert = $nouvellesactions->sqlstring($idFacture);
                        $base->exec($requeteinsert);

                        $n++;

                    }

                    echo       "<div class=\"row\">
                                    <div class=\"col-lg-12\">
                                        <div class=\"alert alert-success\">
                                        Prestations ajoutées avec succès !
                                        </div>
                                    </div>
                                </div>";


                }


                if($_POST['dataPrestExistante'] == "NULL" && $_POST['dataNbPrestation'] != NULL){
                    $nbPrestationTotal = $_POST['dataNbPrestation'] + 1;
                    $n = 1;
                    while ($n < $nbPrestationTotal) {

                        $design = $_POST["dataDesignation".$n.""];
                        $quante = $_POST["dataQte".$n.""];
                        $uni = $_POST["dataUnitaire".$n.""];
                        $tot = $_POST["dataTotal".$n.""];


                        $nouvellesactions = new actionfacture($design, $quante, $uni, $tot);
                        $requeteinsert = $nouvellesactions->sqlstring($idFacture);
                        $base->exec($requeteinsert);

                        $n++;
                    }
                    echo "
                                <div class=\"row\">
                                    <div class=\"col-lg-12\">
                                        <div class=\"alert alert-success\">
                                        Prestations ajoutées avec succès !
                                        </div>
                                    </div>
                                </div>";


                }

                // FIN INSERT INTO

            }
            catch(PDOException $e)
            {
                echo "
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                <div class=\"alert alert-danger\">
                                Erreur updates !<br><br>" . $e->getMessage().
                                "</div>
                            </div>
                        </div>";
            }

            echo "
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        <div class=\"alert alert-success\">
                        Facture n°". $_POST['dataNum']. " modifiée avec succès !
                        </div>
                    </div>
                </div>";

        }
        catch(PDOException $e) {
            echo "
                    <div class=\"row\">
                        <div class=\"col-lg-12\">
                            <div class=\"alert alert-danger\">
                            Erreur !<br><br>". $requetefact . "<br><br>" . $e->getMessage() .
                            "</div>
                        </div>
                    </div>";
    }
}


    // Fonctions Styles

function affichernbfacture($base){

    $requete = "SELECT COUNT(idFacture) AS nombre FROM factures";
    $affichage = $base->prepare($requete);
    $affichage->execute();

    while ($donnees = $affichage->fetch()) {
        return $donnees['nombre'];
    }
}

function afficherles($bdd){
    $nb = affichernbfacture($bdd);
    if ($nb > 1){
        echo 's';
    }
    else{
        echo '';
    }
}


    // Fonctions Tableaux

function tableaufacture($base){


    echo '
        <div class="row">
            <div class="col-lg-12">
                <!-- /.panel -->
                <div class="row">

                    <div class="table-responsive">

                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Numéro de la facture</th>
                                <th>Nom du client</th>
                                <th>Adresse du client</th>
                                <th>Date de création de la facture</th>
                                <th>Ville</th>
                                <th id="actionsTableau">Actions</th>
                            </tr>
                            </thead>
                            <tbody>';

    $requete = "SELECT * FROM factures";

    $affichage = $base->prepare($requete);
    $affichage->execute();

    $num = 0;
    while ($donnees = $affichage->fetch()) {

        echo
            '                   
                                <tr id="rowFacture">
                                    <form id="formModif'.$num.'" action="modif.php" method="post">
                                    <input type="hidden" name="dataIDFacture" id="inputIDFacture'.$num.'" value=\''. $donnees['idFacture'] .'\'>
                                    </form>
                                    <form id="formPrint'.$num.'" action="impress.php" method="post">
                                    <input type="hidden" name="dataIDFacture" id="inputIDFacture'.$num.'" value=\''. $donnees['idFacture'] .'\'>
                                    </form>
                                    <form id="formSuppr'.$num.'" action="suppr.php" method="post">
                                    <input type="hidden" name="dataIDFacture" id="inputIDFacture'.$num.'" value=\''. $donnees['idFacture'] .'\'>
                                    </form>
                                    <td onclick="SubmitTableau(\'formModif'.$num.'\')">' . $donnees['num'] . '</td>
                                    <td onclick="SubmitTableau(\'formModif'.$num.'\')">' . $donnees['nomClient'] . '</td>
                                    <td onclick="SubmitTableau(\'formModif'.$num.'\')">' . $donnees['adresse'] .   '</td>
                                    <td onclick="SubmitTableau(\'formModif'.$num.'\')">' . preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1", $donnees['date']) .      '</td>
                                    <td onclick="SubmitTableau(\'formModif'.$num.'\')">' . $donnees['ville'] .     '</td>
                                    <td id="actionsTableau">
                                        <button class="btn btn-danger btn-circle" data-toggle="modal" data-target="#ModalSupprimer"><i class="fa fa-trash"></i></button>        
                                        <button type="submit" form="formPrint'.$num.'"  class="btn btn-warning btn-circle" data-toggle="modal"><i class="fa fa-print"></i></button>
                                        <button type="submit" form="formModif'.$num.'"  class="btn btn-info btn-circle" data-toggle="modal"><i class="fa fa-pencil"></i></button>
                                    </td>
                                </tr>
                                ';
        $num ++;
    }

    echo '
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                    <!-- /.col-lg-8 (nested) -->
                </div>
                <!-- /.row -->

                <!-- /.panel -->
            </div>
            <!-- /.col-lg-8 -->

            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->';

}

function tableaufacturelock($base){


    $requete = "SELECT * FROM factures";

    $affichage = $base->prepare($requete);
    $affichage->execute();

    echo '
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-table fa-fw"></i> Toutes les factures
                    </div>
                <!-- /.panel -->

                    <div class="row" id="rowtableau">

                    <div class="table-responsive">

                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Numéro de la facture</th>
                                <th>Nom du client</th>
                                <th>Adresse du client</th>
                                <th>Date de création de la facture</th>
                                <th>Ville</th>
                            </tr>
                            </thead>
                            <tbody>
        ';

    $num = 0;

    while ($donnees = $affichage->fetch()) {

        echo
            '                   <tr id="rowFacture" onClick="SubmitTableau(\'formModif'.$num.'\');">
                                    <form id="formModif'.$num.'" action="modif.php" method="post">
                                    <td>'. $donnees['num'].'</td>
                                    <td>'. $donnees['nomClient'].'</td>
                                    <td>'. $donnees['adresse'].'</td>
                                    <td>'. $donnees['date'] .'</td>
                                    <td>'. $donnees['ville'].'</td>
                                    <input type="hidden" name="dataIDFacture" id="inputIDFacture'.$num.'" value=\''. $donnees['idFacture'] .'\'>
                                    </form>
                                </tr>';
        $num ++;
    }




    echo '
                                    </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                    <!-- /.col-lg-8 (nested) -->
                </div>
                <!-- /.row -->

                <!-- /.panel -->
            </div>
            <!-- /.col-lg-8 -->

            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
        ';

}

function listefacture($base){

    $requete = "SELECT * FROM factures";

    $affichage = $base->prepare($requete);
    $affichage->execute();

    echo '<div id="divTableNav" class="table-responsive">

                        <table id="TableNav" class="table table-bordered table-hover">
                            <thead id="NoBorder">
                            <tr id="NoBorderTableau">
                                <th id="NoBorder">Numéro</th>
                                <th id="NoBorder">Client</th>
                            </tr>
                            </thead>
                            <tbody id="NoBorder">
    ';

    $num = 0;
    while ($donnees = $affichage->fetch()) {

        echo
            '<tr id="NoBorder" onClick="SubmitTableau(\'formModif'.$num.'\')">
                                    <form id="formModif'.$num.'" action="modif.php" method="post">
                                    <td id="NoBorder">'. $donnees['num'].'</td>
                                    <td id="NoBorder">'. $donnees['nomClient'].'</td>
                                    <input type="hidden" name="dataIDFacture" id="inputIDFacture'.$num.'" value=\''. $donnees['idFacture'] .'\'>
                                    </form>
            </tr>';
        $num ++;
    };

    echo '
                                    </tbody>
                        </table>
                    </div>';

}


    // Fonction Form


function getactionsfacture($base, $id) {

        //  Requete pour obtenir le nombre de pretations de la facture
    $req = "SELECT COUNT(idAction) AS nombre FROM actionsfactures WHERE idFacture =". $id . ";";
    $affich = $base->prepare($req);
    $affich->execute();
    $result = $affich->fetch();
    $nbactions = ($result['nombre']);


    $requete = "SELECT * FROM actionsfactures WHERE idFacture =". $id . ";";
    $affichage = $base->prepare($requete);
    $affichage->execute();
    $compteur = 0;
    while ($donnees = $affichage->fetch()) {


        echo "<div class=\"panel panel-default\" id=\"prestcontainer". $compteur ."\">";
        if ($compteur > 0 && $compteur == ($nbactions-1)){ echo "<div id=\"close" . $compteur . "\"><button type = \"button\" id = \"btnclose".$compteur."\" class=\"btn btn-danger btn-circle\" style = \"position: absolute; right: 0px; padding-top: 6px;\" 
        onclick = 'i=i-1; addButtonExist(".$donnees['idAction']."); removeParent();' ><i class=\"fa fa-times\"></i></button ></div>";} else {echo "<div id=\"close". $compteur ."\"></div>";};
        echo "<!-- /.panel-heading -->
            <div class=\"panel-body\">
                <div class=\"row\">
                    <div class=\"col-xs-8 col-sm-8 col-md-8 col-lg-8\">
                        <div class=\"row\">
                            <!-- /.form-group -->
                            <div class=\"form-group col-lg-12\">
                                <label for=\"inputDesignation".$compteur."\">Désignation de la prestation "; if($compteur > 0){echo $compteur + 1;}; echo "</label>
                                <textarea type=\"text\" class=\"form-control\" id=\"inputDesignation".$compteur."\" name=\"dataDesignationExist"; if($compteur > 0){echo $compteur;}; echo "\" placeholder=\"Désignation\" required>" .  $donnees['designation'] . "</textarea>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xs-3 col-xs-3 col-md-3 col-lg-2\">
                        <div class=\"row\" id=\"formchiffres\">
                            <div class=\"col-lg-12\">
                                
                                <div>
                                    <label id=\"LabelForms\" for=\"inputUnitaire".$compteur."\">Prix unitaire</label>
                                </div>
                                <div class=\"input-group col-lg-12\">
                                    <span class=\"input-group-addon\">€</span>
                                    <input type=\"number\" min=\"0\" step=\"0.01\" data-number-to-fixed=\"2\" data-number-stepfactor=\"100\" class=\"form-control currency\" id=\"inputUnitaire".$compteur."\" name = \"dataUnitaireExist"; if($compteur > 0){echo $compteur;}; echo "\" placeholder = \"Unitaire\" value = '"; echo $donnees['prixUnitaire']; echo "' >
                                </div>
                            
                                <div>
                                    <label id=\"LabelForms\" for=\"inputQuant".$compteur."\">Quantité</label>
                                </div>
                                <div class=\"input-group col-lg-12\">
                                    <input type=\"number\" min=\"1\" step=\"1\" data-number-to-fixed=\"2\" data-number-stepfactor=\"100\" class=\"form-control currency\" id=\"inputQuant".$compteur."\" name=\"dataQteExist"; if($compteur > 0){echo $compteur;}; echo "\" placeholder=\"Quantité\" value='". $donnees['qte'] ."'>
                                </div>
                                
                                <div>
                                    <label id=\"LabelForms\" for=\"inputTotal".$compteur."\">Prix total</label>
                                </div>
                                <div class=\"input-group col-lg-12\">
                                    <span class=\"input-group-addon\">€</span>
                                    <input type=\"number\" min=\"0\" step=\"0.01\" data-number-to-fixed=\"2\" data-number-stepfactor=\"100\" class=\"form-control currency\" id=\"inputTotal".$compteur."\" name=\"dataTotalExist"; if($compteur > 0){echo $compteur;}; echo "\" placeholder=\"Total\" value='" . $donnees['prix'] . "' required>
                                </div>
                                
                                <input type=\"hidden\" name=\"dataIDAction"; if($compteur > 0){echo $compteur;}; echo "\" id=\"inputIDAction".$compteur."\" value='". $donnees['idAction'] ."'>
                            </div>
                        </div>
                    </div>
                </div>
         <!-- </div> -->
        </div></div>";
        $compteur ++;
    };
    $compteur -= 1;
    echo "<input type=\"hidden\" name=\"dataNbPrestation\" id=\"inputNbPrestation\">";

    echo "<script>i =".($compteur+1)."</script>";
    echo "<script>document.getElementById(\"inputNbPrestation\").value ="; if ($compteur==0){echo "NULL";}else{echo $compteur;} ; echo ";</script>";

    echo "<input type=\"hidden\" name=\"dataPrestExistante\" id=\"inputPrestExistante\" value='"; if ($compteur==0){echo "NULL";}else{echo $compteur;} ; echo "'>";
    echo "<input type=\"hidden\" name=\"dataNbSupprim\" id=\"inputNbSupprim\" value = 0>";
    echo "<input type=\"hidden\" name=\"dataIDFacture\" id=\"inputIDFacture\" value='".$_POST['dataIDFacture']."'>";


}


function selectauteur($base){

    try {
        $requete = "SELECT `valeur` FROM `params` WHERE `nom` = 'nomFacture'";

        $affichage = $base->prepare($requete);
        $affichage->execute();

        echo '<div class="form-group col-md-6 col-lg-3">
            <label for="selectAuteur">Auteur</label>
                <select id="selectAuteur" class="form-control" name="dataAuteur">';

        while ($donnees = $affichage->fetch()){
            echo '<option value="'.$donnees['valeur'].'">'. $donnees['valeur']. '</option>';
        }
        echo '</select>
    </div>';
    }
catch(PDOException $e)
    {
        echo $requete . "<br>" . $e->getMessage();
    }
}

function getinfosauteur($base){
    try {
        $requete = "SELECT `valeur` FROM `params`";

        $affichage = $base->prepare($requete);
        $affichage->execute();
        $z=0;

        while ($donnees = $affichage->fetch()){
            if($z==0){echo '<div id="BoldNames">';};
            if($z==2){echo 'SIRET : ';};
            echo $donnees['valeur'];
            if($z==0){echo '</div>';} else {echo '<br>';};
            $z ++;
        }
    }
    catch(PDOException $e)
    {
        echo $requete . "<br>" . $e->getMessage();
    }
}

function getinfofacture($base, $id){

    $requete = "SELECT * FROM factures WHERE idFacture =". $id . ";";
    $affichage = $base->prepare($requete);
    $affichage->execute();
    $donnees = $affichage->fetch();
    return $donnees;
}

function getinfoactionsfacture($base, $id){

    $requete = "SELECT * FROM actionsfactures WHERE idFacture =". $id . ";";
    $affichage = $base->prepare($requete);
    $affichage->execute();
    $donnees = $affichage->fetch();
    return $donnees;
}

function gettablectionsfacture($base, $id){

    $requete = "SELECT * FROM actionsfactures WHERE idFacture =". $id . ";";
    $affichage = $base->prepare($requete);
    $affichage->execute();
    while ($donnees = $affichage->fetch()) {
        echo "<tr>
                                <td id='PrintTableGauche'>".$donnees['designation']."</td>
                                <td>".$donnees['qte']." </td>
                                <td>".$donnees['prixUnitaire']." €</td>
                                <td>".$donnees['prix']." €</td>
                            </tr>";
    }
}

function gettotalht($base, $id){

    $requete = "SELECT prix FROM actionsfactures WHERE idFacture =". $id . ";";
    $affichage = $base->prepare($requete);
    $affichage->execute();
    $var = 0;
    while ($donnees = $affichage->fetch()) {
        $var = $var + $donnees{'prix'};
    }
    echo $var;
}
