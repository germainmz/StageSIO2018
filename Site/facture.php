    <!-- Navigation -->
    <?php $nompage = 'Ajout';?>
    <?php include 'templates/header.php'?>
    <script>var i = 1;</script>

    <!-- /.navbar-header -->

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ajout de factures | Formulaire</h1>
            </div>
        </div>

        <form action="ajout.php" method="post">
                <!-- /.panel-heading -->
                <div class="" id="panelFacture">

                    <!-- /.panel-Infos -->
                    <div class="">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-user fa-fw"></i> Informations du client
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <div class="row">
                                    <!-- /.form-group -->
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputNom">Nom et Prénom</label>
                                        <input type="text" class="form-control" id="inputNom" name="dataNom" placeholder="Nom" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputAdresse">Adresse</label>
                                        <input type="text" class="form-control" id="inputAdresse" name="dataAdresse" placeholder="Adresse" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputVille">Ville</label>
                                        <input type="text" class="form-control" id="inputVille" name="dataVille" placeholder="Ville" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputCP">Code postal</label>
                                        <input type="number" min="10000" max="99999" class="form-control" id="inputCP" name="dataCP" placeholder="CP" required>
                                    </div>
                                    <div class="form-group col-md-12 col-lg-4">
                                        <label for="inputContact">Contact</label>
                                        <input type="text" class="form-control" id="inputContact" name="dataContact" placeholder="Contact" required>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.list-group -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>



                    <div class="">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-user fa-fw"></i> Informations de la facture
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">

                                            <div class="row">
                                                <!-- /.form-group -->
                                                <div class="form-group col-md-4 col-lg-2">
                                                    <label for="inputFactDate">Date</label>
                                                    <input type="date"class="form-control" name="dataDate" id="inputFactDate" required>
                                                </div>
                                                <div class="form-group col-md-8 col-lg-4">
                                                    <label for="inputNumFact">Numéro de facture</label>
                                                    <input type="text" class="form-control" name="dataNum" id="inputNumFact" placeholder="Numéro de facture" required>
                                                </div>
                                                <?php selectauteur($bdd)?>
                                                <div class="form-group col-md-6 col-lg-2">
                                                    <label for="selectType">Type</label>
                                                    <select id="selectType" class="form-control" name="dataType">
                                                        <option value="Facture">Facture</option>
                                                        <option value="Avoir">Avoir</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->

                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-group col-md-12 col-lg-6">
                                                    <label for="inputIntitule">Intitulé</label>
                                                    <input type="text" class="form-control" id="inputIntitule" name="dataIntitule" placeholder="Intitulé" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-lg-12" id="#additionalElements">

                                    <!-- <div class="col-lg-12"> -->
                                        <div class="panel panel-default">
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-7 col-md-8 col-lg-8">
                                                        <div class="row">
                                                            <!-- /.form-group -->
                                                            <div class="form-group col-lg-12">
                                                                <label for="inputDesignation">Désignation de la prestation</label>
                                                                <textarea type="text" class="form-control" rows="3" id="inputDesignation" name="dataDesignation" placeholder="Désignation" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-xs-5 col-md-4 col-lg-4">
                                                        <div class="row" id="formchiffres">
                                                            <div class="col-lg-12">

                                                                <div>
                                                                    <label id="LabelForms" for="inputUnitaire">Prix unitaire</label>
                                                                </div>
                                                                <div class="input-group col-lg-12">
                                                                    <span class="input-group-addon">€</span>
                                                                    <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="inputUnitaire" name = "dataUnitaireExist" placeholder = "Unitaire">
                                                                </div>

                                                                <div>
                                                                    <label id="LabelForms" for="inputQuant">Quantité</label>
                                                                </div>
                                                                <div class="input-group col-lg-12">
                                                                    <input type="number" min="1" step="1" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="inputQuant" name="dataQteExist" placeholder="Quantité">
                                                                </div>

                                                                <div>
                                                                    <label id="LabelForms" for="inputTotal">Prix total</label>
                                                                </div>
                                                                <div class="input-group col-lg-12">
                                                                    <span class="input-group-addon">€</span>
                                                                    <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="inputTotal" name="dataTotalExist" placeholder="Total" value='' required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                                    <!-- /.panel-body -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button onclick="addLine('#additionalElements')" type="button" class="btn btn-success col-lg-offset-9 col-lg-3">
                                            Ajouter une prestation
                                        </button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.panel -->
                        </div>

                    </div>

                    <div class="">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-user fa-fw"></i> Détails
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <div class="row">
                                    <!-- /.form-group -->
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputDebut">Date de début</label>
                                        <input type="text" class="form-control" id="inputDebut" name="dataDebut" placeholder="Début" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputFin">Date de fin</label>
                                        <input type="text" class="form-control" id="inputFin" name="dataFin" placeholder="Fin" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="selectReglement">Réglement</label>
                                        <select id="selectReglement" class="form-control" name="dataReglement">
                                            <option value="Paiement à réception, par PayPal">Paiement à réception, par PayPal</option>
                                            <option value="option2">option2</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-2">
                                        <label for="inputEcheance">Echéance paiement</label>
                                        <input type="date" class="form-control" id="inputEcheance" name="dataEcheance"
                                               placeholder="Echéance paiement" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-8">
                                        <label for="inputInfo">Options</label>
                                        <textarea type="text" rows="5" class="form-control" id="inputOptions" name="dataOptions"
                                                  placeholder="Options" required></textarea>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.list-group -->

                            </div>
                            <!-- /.panel-body -->

                        </div>

                            <div class="panel panel-default">

                                <!-- /.panel-heading -->
                                <div class="panel-body" id="panelValider">
                                    <div class="row">
                                        <input type="hidden" name="dataNbPrestation" id="inputNbPrestation" value=''>
                                        <input type="submit" class="btn btn-success col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                                    </div>
                                </div>
                            </div>

                        <!-- /.panel -->
                    </div>
                </div>

            <!-- /.panel -->
        </form>


<?php tableaufacturelock($bdd)?>

    </div>

</div>
<!-- /#page-wrapper -->


<!-- Modal -->
<div class="modal fade" id="Modifier" tabindex="-1" role="dialog" aria-labelledby="Modifier" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modifier une facture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Test de contenu.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/main.js"></script>
<script>i = i + 1;</script>

</body>

</html>
