    <!-- Navigation -->
    <?php $nompage = 'Accueil';?>
    <?php include 'templates/header.php'?>
    <!-- <body> -->
    <!-- /.navbar-header -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Gestion des factures</h1>
            </div>
        </div>


        <?php tableaufacture($bdd)?>

        <center><?php echo affichernbfacture($bdd)?> facture<?php afficherles($bdd)?>.</center>

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



<!-- Modal -->
<div class="modal fade" id="ModalSupprimer" tabindex="-1" role="dialog" aria-labelledby="ModalSupprimer" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <b>Supprimer la facture n°</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir supprimer cette facture ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                <button type="button" class="btn btn-primary">Oui</button>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/main.js"></script>

</body>

</html>