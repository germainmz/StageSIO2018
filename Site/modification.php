<!-- Navigation -->
<?php $nompage = 'Facture modifiée';?>
<?php include 'templates/header.php'?>

    <!-- /.navbar-header -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Modification de facture | Finalisation</h1>
            </div>
        </div>

        <?php modiffactures($bdd);?>


        <?php tableaufacturelock($bdd);?>

    </div>

</div>
<!-- /#page-wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/main.js"></script>

</body>

</html>
