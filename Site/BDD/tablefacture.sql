-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 30 mai 2018 à 07:11
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tablefacture`
--

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

DROP TABLE IF EXISTS `factures`;
CREATE TABLE IF NOT EXISTS `factures` (
  `FACNum` int(10) NOT NULL COMMENT 'Code de la facture',
  `FACNomClient` text COLLATE utf8_bin NOT NULL COMMENT 'Nom du client',
  `FACPrenomClient` text COLLATE utf8_bin NOT NULL COMMENT 'Prénom du client',
  `FACDate` date NOT NULL COMMENT 'Date de la facture',
  `FACDétail` text COLLATE utf8_bin NOT NULL COMMENT 'Détail de la facture',
  PRIMARY KEY (`FACNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `factures`
--

INSERT INTO `factures` (`FACNum`, `FACNomClient`, `FACPrenomClient`, `FACDate`, `FACDétail`) VALUES
(1, 'LeNom', 'LePrénom', '2018-04-11', 'Il s\'agit de la première facture !');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
