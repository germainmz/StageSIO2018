<!-- Navigation -->
<?php $nompage = 'Aperçu';?>
<?php include 'templates/header.php'?>

<link rel="stylesheet" type="text/css" media="print" href="css/print.css">

<?php $resultatfacture = getinfofacture($bdd, $_POST['dataIDFacture']); $resultatactionsfactures = getinfoactionsfacture($bdd, $_POST['dataIDFacture']); ?>
<!-- /.navbar-header -->

<div id="page-wrapper">
    <div class="row" id="TitlePage">
        <div class="col-lg-12">
            <h1 class="page-header">Impression de la facture n°<?php echo $resultatfacture['num'];?></h1>
        </div>
    </div>


    <!-- /header print-->
    <div class="row" id="rowHeader">
        <div id="PrintInfos" class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <img id="PrintLogo" src="data/Logo.png">
        </div>
        <div id="PrintFactAvoir" class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-lg-4">
            <?php echo $resultatfacture['factAvoir'];?>
        </div>
    </div>

    <div class="row" id="rowNoms">
        <div id="PrintAuteur" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?php echo getinfosauteur($bdd);?>
        </div>
        <div id="PrintClients" class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div id="BoldNames"><?php echo $resultatfacture['nomClient'];?></div>
            <?php echo $resultatfacture['adresse'];?><br>
            <?php echo $resultatfacture['cp'];?> <?php echo $resultatfacture['ville'];?><br>
        </div>
    </div>

    <div class="row" id="rowDispense">
        <div id="PrintDispense" class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            Dispensé d’immatriculation au registre du commerce et des sociétés et au répertoire des métiers.
        </div>
    </div>

    <div class="row" id="rowNums">
        <div id="PrintNumDate" class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <b>N° Facture : </b><?php echo $resultatfacture['num'];?><br>
            <b>Date : </b><?php echo preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1", $resultatfacture['date'])?>
        </div>
    </div>

    <div class="row" id="rowIntitule">
        <div id="PrintIntitule" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <b>Intitulé : </b><?php echo $resultatfacture['intitule'];?><br>
        </div>
    </div>

    <div class="row" id="rowTable">
        <div id="PrintTableau" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="table-responsive" id="PrintTableResponsive">
                    <table class="table table-bordered table-hover table-striped" id="PrintTable">
                        <thead>
                            <tr id="PrintTr">
                                <th>Désignation</th>
                                <th>Qté (en h)</th>
                                <th>PU</th>
                                <th>Prix total en HT</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php gettablectionsfacture($bdd, $_POST['dataIDFacture'])?>
                            <tr>
                                <td id="PrintTableVide"></td>
                                <td id="PrintTableVide"></td>
                                <td id="PrintTr">Remise</td>
                                <td id="PrintTableBas">- ?? €</td>
                            </tr>
                            <tr>
                                <td id="PrintTableVide"></td>
                                <td id="PrintTableVide"></td>
                                <td id="PrintTr">Total HT</td>
                                <td id="PrintTableBas"><b><?php gettotalht($bdd, $_POST['dataIDFacture'])?> €</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

        </div>
    </div>


    <div class="row" id="rowTVA">
        <div id="PrintTVA" class="col-xs-offset-8 col-sm-offset-8 col-md-offset-8 col-lg-offset-8 col-xs-4 col-sm-4 col-md-4 col-lg-4">
            TVA	non	applicable,	art.293	B du CGI
        </div>
    </div>

    <div class="row" id="rowInfos">
        <div id="PrintDebut" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <b>Date de début de prestation : </b><?php echo $resultatfacture['dateDeb'];?>.<br>
        </div>
        <div id="PrintFin" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <b>Date de fin estimée : </b><?php echo $resultatfacture['dateFin'];?>.<br>
        </div>
        <div id="PrintReglement" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <b>Condition de règlement : </b><?php echo $resultatfacture['condReglement'];?>.<br>
        </div>
        <div id="PrintEcheance" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <b>Echéance de paiement : </b><?php echo $resultatfacture['echeance'];?>.<br>
        </div>
        <div id="PrintPenalite" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            Taux de pénalité de retard : ???.
        </div>
        <div id="PrintEscompte" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            Escompte : ???.
        </div>
    </div>



</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/main.js"></script>

</body>

</html>