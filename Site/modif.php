<!-- Navigation -->
<?php $nompage = 'Modification';?>
<?php include 'templates/header.php'?>
<?php $resultat = getinfofacture($bdd, $_POST['dataIDFacture']); ?>
<!-- /.navbar-header -->



<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="js/main.js"></script>



<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Modification de facture | N°<?php echo $resultat['num'];?></h1>
        </div>
    </div>

    <form action="modification.php" method="post">
        <!-- /.panel-heading -->
        <div class="" id="panelFacture">

            <!-- /.panel-Infos -->
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user fa-fw"></i> Informations du client
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">

                        <div class="row">
                            <!-- /.form-group -->
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputNom">Nom et Prénom</label>
                                <input type="text" class="form-control" id="inputNom" name="dataNom" placeholder="Nom" value="<?php echo $resultat['nomClient']?>" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputAdresse">Adresse</label>
                                <input type="text" class="form-control" id="inputAdresse" name="dataAdresse" placeholder="Adresse" value="<?php echo $resultat['adresse']?>" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputVille">Ville</label>
                                <input type="text" class="form-control" id="inputVille" name="dataVille" placeholder="Ville" value="<?php echo $resultat['ville']?>" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputCP">Code postal</label>
                                <input type="number" min="10000" max="99999" class="form-control" id="inputCP" name="dataCP" value="<?php echo $resultat['cp']?>" placeholder="CP" required>
                            </div>
                            <div class="form-group col-md-12 col-lg-4">
                                <label for="inputContact">Contact</label>
                                <input type="text" class="form-control" id="inputContact" name="dataContact" value="<?php echo $resultat['contact']?>" placeholder="Contact" required>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>



            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user fa-fw"></i> Informations de la facture
                    </div>
                    <div class="panel-body">

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <!-- /.panel-heading -->
                                <div class="panel-body">

                                    <div class="row">
                                        <!-- /.form-group -->
                                        <div class="form-group col-md-4 col-lg-2">
                                            <label for="inputFactDate">Date</label>
                                            <input type="date"class="form-control" name="dataDate" id="inputFactDate"  value="<?php echo $resultat['date']?>" required>
                                        </div>
                                        <div class="form-group col-md-8 col-lg-4">
                                            <label for="inputNumFact">Numéro de facture</label>
                                            <input type="text" class="form-control" name="dataNum" id="inputNumFact" value="<?php echo $resultat['num']?>" placeholder="Numéro de facture" required>
                                        </div>
                                        <?php selectauteur($bdd)?>
                                        <div class="form-group col-md-6 col-lg-2">
                                            <label for="selectType">Type</label>
                                            <select id="selectType" class="form-control" name="dataType" value="<?php echo $resultat['factAvoir']?>">
                                                <option value="Facture">Facture</option>
                                                <option value="Avoir">Avoir</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-12 col-lg-6">
                                            <label for="inputIntitule">Intitulé</label>
                                            <input type="text" class="form-control" id="inputIntitule" name="dataIntitule" placeholder="Intitulé" value="<?php echo $resultat['intitule']?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-12" id="#additionalElements">
                            <?php getactionsfacture($bdd, $_POST['dataIDFacture'])?>
                        </div>
                        <!-- /.panel-body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <button onclick="addLineExist('#additionalElements')" type="button" class="btn btn-success col-lg-offset-9 col-lg-3">
                                    Ajouter une prestation
                                </button>
                            </div>
                        </div>

                    </div>
                    <!-- /.panel -->
                </div>

            </div>

            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-user fa-fw"></i> Détails
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">

                        <div class="row">
                            <!-- /.form-group -->
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputDebut">Date de début</label>
                                <input type="text" class="form-control" id="inputDebut" name="dataDebut" placeholder="Début" value="<?php echo $resultat['dateDeb']?>" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputFin">Date de fin</label>
                                <input type="text" class="form-control" id="inputFin" name="dataFin" placeholder="Fin" value="<?php echo $resultat['dateFin']?>" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="selectReglement">Réglement</label>
                                <select id="selectReglement" class="form-control" name="dataReglement" value="<?php echo $resultat['condReglement']?>">
                                    <option value="option1">option1</option>
                                    <option value="option2">option2</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-lg-2">
                                <label for="inputEcheance">Echéance paiement</label>
                                <input type="date" class="form-control" id="inputEcheance" name="dataEcheance" value="<?php echo $resultat['echeance']?>"
                                       placeholder="Echéance paiement" required>
                            </div>
                            <div class="form-group col-md-6 col-lg-8">
                                <label for="inputInfo">Options</label>
                                <textarea type="text" rows="5" class="form-control" id="inputOptions" name="dataOptions"
                                          placeholder="Options" required><?php echo $resultat['options']?></textarea>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.list-group -->

                    </div>
                    <!-- /.panel-body -->

                </div>

                <div class="panel panel-default">

                    <!-- /.panel-heading -->
                    <div class="panel-body" id="panelValider">
                        <div class="row">
                            <button type="submit" class="btn btn-success col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">Modifier</button>
                        </div>
                    </div>
                </div>

                <!-- /.panel -->
            </div>
        </div>

        <!-- /.panel -->
    </form>


    <?php tableaufacturelock($bdd)?>

</div>

</div>
<!-- /#page-wrapper -->


<!-- Modal -->
<div class="modal fade" id="Modifier" tabindex="-1" role="dialog" aria-labelledby="Modifier" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modifier une facture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Test de contenu.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-primary">Enregistrer</button>
            </div>
        </div>
    </div>
</div>

</body>

</html>
