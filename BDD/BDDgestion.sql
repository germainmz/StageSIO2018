-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  mer. 30 mai 2018 à 15:29
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `BDDgestion`
--

-- --------------------------------------------------------

--
-- Structure de la table `actionsFactures`
--

CREATE TABLE `actionsFactures` (
  `idAction` int(11) NOT NULL,
  `idFacture` int(11) NOT NULL,
  `designation` varchar(5000) DEFAULT NULL,
  `qte` int(11) DEFAULT NULL,
  `prixUnitaire` decimal(10,0) DEFAULT NULL,
  `prix` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `actionsFactures`
--

INSERT INTO `actionsFactures` (`idAction`, `idFacture`, `designation`, `qte`, `prixUnitaire`, `prix`) VALUES
(2, 1, 'Mise à jour graphique du site internet http://www.marie-marcy.com/ :\r\n- Mise à jour graphique pour correspondre à la maquette.', NULL, NULL, '300');

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

CREATE TABLE `factures` (
  `idFacture` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `date` date NOT NULL,
  `nomClient` varchar(200) NOT NULL,
  `adresse` varchar(500) NOT NULL,
  `cp` int(5) NOT NULL,
  `ville` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `intitule` varchar(500) NOT NULL,
  `dateDeb` varchar(500) NOT NULL,
  `dateFin` varchar(500) NOT NULL,
  `condReglement` varchar(500) NOT NULL,
  `echeance` varchar(500) NOT NULL,
  `options` varchar(500) NOT NULL,
  `factAvoir` varchar(10) NOT NULL DEFAULT 'facture'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `factures`
--

INSERT INTO `factures` (`idFacture`, `num`, `date`, `nomClient`, `adresse`, `cp`, `ville`, `contact`, `intitule`, `dateDeb`, `dateFin`, `condReglement`, `echeance`, `options`, `factAvoir`) VALUES
(1, 20180302, '2018-03-23', 'Marie Marcy', '47, chemin du Vieux Chêne', 38240, 'Meylan\r\n ', '', 'Modifications graphiques du site internet http://www.marie-marcy.com/', 'Au jour de la signature du devis.', '2 semaines après la signature du devis.', 'Paiement à réception, par PayPal', 'à réception.', 'Taux de pénalité de retard : 11% et indemnité de 40€ HT pour frais de recouvrement Pas d’escompte pour paiement anticipé', 'facture');

-- --------------------------------------------------------

--
-- Structure de la table `params`
--

CREATE TABLE `params` (
  `nom` varchar(200) NOT NULL,
  `valeur` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `params`
--

INSERT INTO `params` (`nom`, `valeur`) VALUES
('nomFacture', 'Benjamin Eyraud'),
('tel', '06 67 77 56 22'),
('siret', '83224428900019'),
('adresse', '16 Rue d’Aguesseau Lyon, 69007');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `actionsFactures`
--
ALTER TABLE `actionsFactures`
  ADD PRIMARY KEY (`idAction`),
  ADD KEY `idFacture` (`idFacture`);

--
-- Index pour la table `factures`
--
ALTER TABLE `factures`
  ADD PRIMARY KEY (`idFacture`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `actionsFactures`
--
ALTER TABLE `actionsFactures`
  MODIFY `idAction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `factures`
--
ALTER TABLE `factures`
  MODIFY `idFacture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `actionsFactures`
--
ALTER TABLE `actionsFactures`
  ADD CONSTRAINT `actionsfactures_ibfk_1` FOREIGN KEY (`idFacture`) REFERENCES `factures` (`idFacture`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
